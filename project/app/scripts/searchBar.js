var globalQuery = null;
function makeQuery(){
    var type = document.getElementById("sb-type").getElementsByClassName("filter-option-inner-inner")[0].innerText;
    //console.log("type value = " + type);
    var table = document.getElementById("sb-table").getElementsByClassName("filter-option-inner-inner")[0].innerText;
    // console.log("table = " + table);
    //var a = "" + a + "-display";
    var display = document.getElementById("sb-" + table.toLowerCase()).getElementsByClassName("filter-option-inner-inner")[1].innerText;
    // console.log("display = " + display);
    var fields = document.getElementById("sb-" + table.toLowerCase()).getElementsByClassName("filter-option-inner-inner")[0].innerText;
    // console.log("fields = " + fields);
    var searchVar = document.getElementById("search-"+table.toLowerCase()).value;
    // console.log("search value = " + searchVar);
    var limitVar = document.getElementById("limit").value;
    // console.log("limit value = " + limitVar);
    //buid query from here
    var query = "SELECT ";

    //if List
    if(type == "List"){
        //add selected elements in (table)-display
        if (display != "Display"){
            query = query + display + " ";
        }   
        else {
            query = query + "* ";
        } 

        query = query + "FROM " + table.toLowerCase() + "s ";
        //add table
        

        //add constraint if applicable 
        if(fields != "Fields"){
            // console.log("fields:." + fields  + ".");
            //query = query + "WHERE " + fields + " ";
            //add constraint
            //query = query + "= ";
            if(isNaN(parseInt(searchVar, 10))){
                if(searchVar != "") query = query + "WHERE " + fields + " = \"" + searchVar + "\" ";
            }
            else {
                query = query  + "WHERE " + fields + " = " + parseInt(searchVar, 10) + " ";
            }
        }

        //add limit if applicable
        if(limitVar != null){
            if (!(isNaN(parseInt(limitVar, 10))) && parseInt(limitVar, 10) > 0) query = query + "LIMIT " + limitVar;
            //add limit
        }
    }

    //SELECT country, COUNT(*) AS occurrences FROM airports GROUP BY country ORDER BY occurrences DESC LIMIT 1;
    //if highest
    if(type == "Highest"){
        if (fields != "Fields"){
            query = query + fields + ", COUNT(*) AS occurrences FROM " + table.toLowerCase() + "s GROUP BY " + fields + " ORDER BY occurrences DESC LIMIT ";
            if(isNaN(parseInt(limitVar, 10))){
                query = query + "1";
            }
            else{
                query = query + parseInt(limitVar, 10);
            }
        }
    }
    
    query = query + ";";

    if(query == "SELECT ;") query = "SELECT country, COUNT(*) AS occurrences FROM airports GROUP BY country ORDER BY occurrences DESC LIMIT 1;"
    //console.log("query = " + query);
    globalQuery = query;
    return query;
}

function makeDecision(){
    $('#loading-modal').modal('show');
    if(document.getElementById("sb-type").getElementsByClassName("filter-option-inner-inner")[0].innerText == "Trip"){
        //return trip shit here
        //console.log('Sending...');
        if($('#location-dst').val() == "" || $('#location-dst').val() == "Santa" ){
            let a = performance.now();
            ipcRenderer.send('asynchronous-message', {isSpread: true, src: $('#location-src').val(), hops: $('#limit').val()});
            let b = performance.now() - a;
            console.log("Find route took " + b +" milliseconds.");
        }
        else {
            var mapDiv = document.getElementById('mapcoll');
            var toCheck = mapDiv.getAttribute('class');
            if(!toCheck.includes('show')) {
                $(function() {
                    $('#mapToggle').click();
                })
            }
            let a = performance.now();
            ipcRenderer.send('asynchronous-message', {isRoute: true, src: $('#location-src').val(), dst: $('#location-dst').val(), hops: $('#limit').val()});
            let b = performance.now() - a;
            console.log("Find route took " + b +" milliseconds.");
        }
    }
    else {
        myQuery(makeQuery());
        $('#loading-modal').modal('hide');
    }
}