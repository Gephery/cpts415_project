
var table;
var dt;

// Table name is string
function setTable(tablename) {
    table = $(tablename);
    dt = table.DataTable();
}

// results = array of array, the inner array corresponding to the fiels 
// fields = single array of strings
function updateTable(results, fields) {
    // Build column headers
    let headers = '<thead><tr>';
    for (let i = 0; i < fields.length; i++) {
        headers += `<th>${fields[i]}</th>`;
    }
    headers += '</tr></thead>';

    // Build data
    let data = '<tbody>';
    for (let i = 0; i < results.length; i++) {
        let tup = results[i];
        data += '<tr>';
        for (let j = 0; j < tup.length; j++) {
            data += `<td>${tup[j]}</td>`;
        }
        data += '</tr>';
    }
    data += '</tbody>';
    
    dt.destroy();
    table.html(`${headers}${data}`);
    dt = table.DataTable();
    $('#loading-modal').modal('hide');
    //console.log('hide model');
}

exports.setTable = setTable;
exports.updateTable = updateTable;