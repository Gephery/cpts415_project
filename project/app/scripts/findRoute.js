const mysql = require('mysql');
const {
    Worker, isMainThread, parentPort, workerData
} = require('worker_threads');
const path = require('path');

var isRoute = false;
var isSpread = false;

const MAX_HOPS = 100;

var talkback;
var debug;

var m;
var g_hops;
var connection;
var work = []; // array of path, src, dst
var validR = []; // array of maps, {pathL is int, path is array}
var workersFree = []; // Make pool of 12 workers
var workerN = 50;
function _setup(er, res, fields) {
    m = new Map();
    //console.log(res);
    connection.end();
    res.forEach((el) => {
        if (el.srcAirportID != null && el.dstAirportID != null) {
            if (m.get(el['srcAirportID']) == undefined)
                m.set(el['srcAirportID'], []);
            m.get(el['srcAirportID']).push(el);
        }
    });
    // Make workers
    for (let i = 0; i < workerN; i++) {
        makeWorker();
    }
    //console.log('Route setup done...');
}

function setup() {
    connection = mysql.createConnection({
        host: '98.146.204.39',
        port: '3306',
        user: 'readOnlyUser',
        password: 'cpts415-bigData',
        database: 'cs415db'
    });
    connection.query('SELECT srcAirportID, dstAirportID, numStops, routeID FROM routes WHERE 1=1;', _setup);
}

var GrouteToPlace = {};
var GpathToPlace = {};
var Gpath = [];
var Groute = [];
var Gcount = 0;
var GI = 0;
var GpathL = 0;
function _sendResultsPath(er, res, fields) {
    let data = res[0];
    let index = GpathToPlace[data.airportID];
    Gpath[index] = data;
    GI++;
    if (Gcount == GI) {
        connection.end();
        if (isRoute)
            talkback.reply('asynchronous-reply', {isRoute: true, found: true, path: Gpath, route: Groute, pathL: GpathL});
        else
            talkback.reply('asynchronous-reply', {isSpread: true, found: true, path: Gpath, route: Groute, pathL: GpathL});
    }
    
}
function _sendResultsRoute(er, res, fields) {
    let data = res[0];
    let index = GrouteToPlace[data.routeID];
    Groute[index] = {airline: data.airline, routeID: data.routeID};
    GI++;
    if (Gcount == GI) {
        connection.end();
        talkback.reply('asynchronous-reply', {isRoute: true, found: true, path: Gpath, route: Groute, pathL: GpathL});
    }
}

function _sendResultsPathBunch(er, res, fields) {
    //console.log(er);
    //console.log('send bunch!!!');
    connection.end();
    let paths = [];
    res.forEach((re) => {
        paths.push(re);
    });
    talkback.reply('asynchronous-reply', {isSpread: true, found: true, path: paths, route: [], pathL: GpathL});
}

function sendResults(win) {
    Gpath = [];
    Groute = [];
    GI = 0;
    connection = mysql.createConnection({
        host: '98.146.204.39',
        port: '3306',
        user: 'readOnlyUser',
        password: 'cpts415-bigData',
        database: 'cs415db'
    });
    // Get path info
    Gcount = win.path.length + win.route.length;
    GpathL = win.pathL;
    let pathl = win.path;
    for (let i = 0; i < pathl.length; i++) {
        GpathToPlace[pathl[i]] = i;
        connection.query(`SELECT * FROM airports WHERE airportID = ${pathl[i]};`, _sendResultsPath);
    }

    // Get route info
    let routel = win.route;
    for (let i = 0; i < routel.length; i++) {
        GrouteToPlace[routel[i]] = i;
        connection.query(`SELECT routeID, airline FROM routes WHERE routeID = ${routel[i]};`, _sendResultsRoute);
    }
}

function sendResultsSpread(wins) {
    Gpath = [];
    Groute = [];
    GI = 0;
    connection = mysql.createConnection({
        host: '98.146.204.39',
        port: '3306',
        user: 'readOnlyUser',
        password: 'cpts415-bigData',
        database: 'cs415db'
    });
    // Get path info
    Gcount = wins.size;
    GpathL = g_hops;
    let i = 0;
    let s = 'SELECT name, airportID, country, city FROM airports WHERE ';
    wins.forEach((win) => {
        GpathToPlace[win] = i;
        if (i == 0)
            s += `airportID = ${win} `;
        else
            s += `|| airportID = ${win}`;
        i++;
    });
    connection.query(s + ';', _sendResultsPathBunch);
}

function makeWorker() {
    let w0 = new Worker(path.resolve(__dirname, '_fRWorker.js'), {workerData: new Map(m)});
    w0.on('exit', () => {
        makeWorker();
        allocateWorkers();
        //console.log('number');
        //console.log(workersFree.length);
        //console.log(work.length);
        if (work.length == 0 && workersFree.length == workerN) {
            let win = validR[0];
            let found = true;
            let winnsP = new Set();
            //console.log(validR);
            //console.log(`g hoooops ${g_hops+1}`);
            validR.forEach((r) => {
                //console.log('1');
                let isGoodNum = r.pathL != -1 && (Number.isNaN(g_hops) || g_hops == -1 || r.pathL < g_hops+1);
                //console.log('2');
                if (isGoodNum && isSpread) {
                    //console.log('3');
                    for (let i = 0; i < r.path.length; i++) {
                        winnsP.add(r.path[i]);
                    }
                    //console.log('4');
                }
                if (isGoodNum && r.pathL < win.pathL) {
                    win = r;
                }
                //console.log(winnsP.size);
            });
            // If no routes then not reachable, or initial path too long
            if (isRoute && (win == undefined || (win.pathL > g_hops && g_hops != -1))) {
                found = false;
            } else if (isRoute) {
                found = win.found;
            }
            //console.log('Done.........');
            //console.log(isSpread);
            if (found && isRoute)
                sendResults(win);
            else if (isSpread && winnsP.size != 0)
                sendResultsSpread(winnsP);
            else if (isSpread)
                talkback.reply('asynchronous-reply', {isSpread: true, found: false, path: []});
            else
                talkback.reply('asynchronous-reply', {isRoute: true, found: isRoute&&found, path: win});
        }
    });
    w0.once('message', (message) => {
        // If work done send winning route
        //console.log('hihihihji');
        //console.log(message);
        
        if (message.isDebug) {
            if (talkback == undefined) {
                //console.log(message.msg);
            } else {
                talkback.reply('asynchronous-reply', message);
            }
            if (message.done) {
                //w0.postMessage({msg: 'hi agian'});
            }
        }
        if (message.isRoute) {
            if (message.found) {
                validR.push(message);
            }
            //makeWorker();
            //workersFree.push(w0);
            //allocateWorkers();
        }
    });
    workersFree.push(w0);
}

// function cont(er, res, fields) {
//     debug = res;
//     w0.postMessage('w0');
// }

function allocateWorkers() {
    while (workersFree.length != 0 && work.length != 0) {
        let worker = workersFree.pop();
        let lWork = work.pop();
        worker.postMessage({isWork: true, path: lWork.path, src: lWork.src, dst: lWork.dst, pathL: lWork.pathL, route: lWork.route, max: lWork.max});
    }
}

function trip(event) {
    isSpread = false;
    isRoute = true;
    validR = [];
    let src = parseInt(event.src);
    let dst = parseInt(event.dst);
    let hops = parseInt(event.hops);
    hops = Number.isNaN(hops) || hops == -1 ? MAX_HOPS : hops;
    g_hops = hops;
    talkback = event.event;
    let firstHs = m.get(src);
    if (firstHs == undefined) {
        talkback.reply('asynchronous-reply', {isRoute: true, found: false, path: []});
    } else {
        //console.log(src);
        //console.log(dst);
        //console.log(firstHs);
        firstHs.forEach((hop) => {
            //console.log(hop.routeID);
            work.push({path: [src], src: hop.dstAirportID, dst: dst, pathL: hop.numStops+1, max: hops+1, route: [hop.routeID]});
        });
        allocateWorkers();
    }
}

function spread(event) {
    isSpread = true;
    isRoute = false;
    validR = [];
    let src = parseInt(event.src);
    let hops = parseInt(event.hops);
    hops = Number.isNaN(hops) || hops == -1 ? MAX_HOPS : hops;
    g_hops = hops;
    talkback = event.event;
    let firstHs = m.get(src);
    if (firstHs == undefined) {
        talkback.reply('asynchronous-reply', {isSpread: true, found: false, path: []});
    } else {
        //console.log(src);
        //console.log(firstHs);
        firstHs.forEach((hop) => {
            //console.log(hop.routeID);
            work.push({path: [src], src: hop.dstAirportID, dst: null, pathL: hop.numStops+1, max: hops+1, route: [hop.routeID]});
        });
        allocateWorkers();
    }
}

exports.trip = trip;
exports.spread = spread;
exports.setup = setup;
