const { parentPort, workerData } = require('worker_threads');

// map of srcAirportID -> {srcAirportID, dstAirportID, numStops, routeID}
var routes;

var GPath = new Set();


// hi = {isMap: true, map: map}
// hi = {isWork: true, path: lWork.path, src: lWork.src, dst: lWork.dst, pathL: lWork.pathL}
// return {found: true, path: [], pathL: int, route: []}
routes = workerData;
parentPort.once('message', (hi) => {
    GPath.clear();
    if (hi.isTest) {
        // console.log(hi);
    }
    if (hi.isMap) {
        routes = hi.map;
    } else if (hi.isWork) {
        let open = new Map();  // {node, {node, weight, past}}
        let visited = new Set(); // just the number
        open.set(hi.src, {weight: hi.pathL, node: hi.src, past: null});
        let cur = open.get(hi.src);
        visited.add(hi.src);
        while (1) {
            if (cur.weight >= hi.max) {
                // console.log('fuck skldfjklsdjf lksdj');
                if (hi.dst != null)
                    parentPort.postMessage({isRoute: true, found: false, path: [], route: [], pathL: -1});
                else
                    parentPort.postMessage({isRoute: true, found: true, path: Array.from(GPath), pathL: 0, route: []});
                return;
            }
            if (hi.dst == null) {
                let hopper = cur;
                while (hopper.past != null) {
                    GPath.add(hopper.node);
                    hopper = hopper.past;
                }
                GPath.add(hopper.node);
            } else if (cur.node == hi.dst) {
                // Build path and return 
                let hopper = cur;
                let path = [];
                let route = [];
                while (hopper.past != null) {
                    path.push(hopper.node);
                    if (hopper.rID != undefined) route.push(hopper.rID);
                    hopper = hopper.past;
                }
                path.push(hopper.node);
                path.reverse();
                route.reverse();
                path.unshift(hi.path[0]);
                route.unshift(hi.route[0]);
                parentPort.postMessage({isRoute: true, found: true, path: path, pathL: cur.weight, route: route});
                //console.log('1111111111111111111111');
                //console.log(cur.node);
                return;
            }
            
            // Add successors
            let sucs = routes.get(cur.node);
            if (sucs == undefined) {
                //console.log('sucs');
                //console.log(cur.node);
                //console.log(cur.past);
                cur = cur.past;
            } else {
                for (let i = 0; i < sucs.length; i++) {
                    let suc = sucs[i];
                    // Add to open if new, or update length if shorter found
                    let w = suc.numStops + cur.weight + 1;
                    if (open.get(suc.dstAirportID) == undefined)
                        open.set(suc.dstAirportID, {weight: w, node: suc.dstAirportID, past: cur, rID: suc.routeID});
                    else {
                        if (w < open.get(suc.dstAirportID).weight) {
                            open.set(suc.dstAirportID, {weight: w, node: suc.dstAirportID, past: cur, rID: suc.routeID});
                        }
                        // else leave as old one
                    }
                }
            }
            
            // Choose new cur
            cur = undefined;
            open.forEach((value, key) => {
                if (!visited.has(value.node) && (cur == undefined || value.weight < cur.weight)) {
                    cur = value;
                }
            });
            // Must be out of nodes
            if (cur == undefined) {
                // console.log('no choice!');

                if (hi.dst != null)
                    parentPort.postMessage({isRoute: true, found: false, path: [], route: [], pathL: -1});
                else
                    parentPort.postMessage({isRoute: true, found: true, path: Array.from(GPath), pathL: 0, route: []});
                return;
            }

            visited.add(cur.node);
        }
    }
});