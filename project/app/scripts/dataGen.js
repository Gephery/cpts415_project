const fs = require('fs');



function genIsSpread(){
    let spreadHeader = "airportName,airportID,nHops,timeToStartWork\n";
    let spreads = [
        {
            a:"1",
            b:"5",
            name:"Goroka"
        },
        {
            a:"3577",
            b:"5",
            name:"SEATAC"
        },
        {
            a:"3797",
            b:"5",
            name:"JFK"
        },
        {
            a:"3944",
            b:"5",
            name:"Pullman"
        },
        {
            a:"340",
            b:"5",
            name:"Frankfurt"
        }
    ];
    //ipcRenderer.send('asynchronous-message', {isSpread: true, src: spreads[i]['a'], hops: j});
    //for each entry
    for(let i = 0; i < spreads.length; i++){

        //for each amount of hops
        for(let j = 1; j <= spreads[i]['b']; j++){
            let outfile = "csvs/" + spreads[i]['name'] + "_hops-" + j + ".csv";
            data = spreadHeader;
            //run test 100 times and write to file at the end
            for(let k = 0; k < 100; k ++){
                let q = performance.now();
                //make call
                ipcRenderer.send('asynchronous-message', {isSpread: true, src: spreads[i]['a'], hops: j.toString()});
                let r = performance.now() - q;
                data += spreads[i]['name'] + ',' + spreads[i]['a'] + ',' + j.toString() + ',' + r.toString() + '\n'; 
            }
            fs.writeFile(outfile, data, (err)=>{
                if(err) throw err;
            });
            //console.log(data);
        }
    }
}

function genIsRoute() {
    let routeHeader = "srcAirport,dstAirport,nHops,timeStartWork\n";
    let routes = [
    {
        a:"1",
        b:"3577",
        c:"5",
        srcAirport:"Goroka",
        dstAirport:"SEATAC"
    },
    {
        a:"3577",
        b:"3797",
        c:"5",
        srcAirport:"SEATAC",
        dstAirport:"JFK"
    },
    {
        a:"3797",
        b:"10",
        c:"5",
        srcAirport:"JFK",
        dstAirport:"Thule Airbase"
    },
    {
        a:"10",
        b:"3944",
        c:"5",
        srcAirport:"Thule Airbase",
        dstAirport:"Pullman"
    },
    {
        a:"3944",
        b:"340",
        c:"5",
        srcAirport:"Pullman",
        dstAirport:"Frankfurt"
    }];
    //ipcRenderer.send('asynchronous-message', {isRoute: true, src: routes[i]['a'], dst: routes[i]['b'], hops: j});
    for(let i = 0; i < routes.length; i++){
        //for each amount of hops
        for(let j = 1; j <= routes[i]['c']; j++){
            let outfile = "csvs/" + routes[i]['srcAirport'] + '-' + routes[i]['dstAirport'] + "_hops-" + j.toString() + ".csv";
            data = routeHeader;

            //run test 100 times and write to file at the end
            for(let k = 0; k < 100; k ++){
                let q = performance.now();
                //make call
                ipcRenderer.send('asynchronous-message', {isRoute: true, src: routes[i]['a'], dst: routes[i]['b'], hops: j.toString()});
                let r = performance.now() - q;
                data += routes[i]['srcAirport'] + ',' + routes[i]['dstAirport'] + ',' + j.toString() + ',' + r.toString() + '\n'; 
            }
            fs.writeFile(outfile, data, (err)=>{
                if(err) throw err;
            });
            //console.log(data);
        }
    }
}