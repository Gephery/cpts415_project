var myMap = L.map('mapid').setView([46.7439,-117.11], 12);
L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox.streets',
    accessToken: 'pk.eyJ1IjoiY2xhbWJlIiwiYSI6ImNrMzljOXZzcTAxazUzbHAxb3RqMzk3ZHoifQ.bk8S-keR8bJroxnGOhEXWA'
}).addTo(myMap);
$(function() {
  $('#mapToggle').click();
});

var mapPopups = L.layerGroup();
var markerList = null;

var connection;
function _startMap(er, res, fields){
  connection.end();
  res.forEach((markers)=>{
    //var markers = res._results[0][i];
    //var keys = Object.keys(markers);
    //console.log("lat = " + markers["airportID"]);
    var marker = L.marker([markers["latitude"], markers["longitude"]]).addTo(myMap);
    marker.bindPopup("<b>" + markers["name"] + "</b><br>airportID = " + markers["airportID"]);
    mapPopups.addLayer(marker);
  });
}

function startMap(){
    connection = mysql.createConnection({
        host: '98.146.204.39',
        port: '3306',
        user: 'readOnlyUser',
        password: 'cpts415-bigData',
        database: 'cs415db'
    });
    connection.query('SELECT * FROM airportMarkers;', _startMap);
}

startMap();

function resetMap() {
  clearMap();
  myMap.addLayer(mapPopups);
}

function clearMap(){
  myMap.remove();
  document.getElementById("mapcoll").innerHTML = "<div id=\"mapid\" class=\"\" style=\"height:270px;\"></div>";
  myMap = L.map('mapid').setView([46.7439,-117.11], 12);
  L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox.streets',
    accessToken: 'pk.eyJ1IjoiY2xhbWJlIiwiYSI6ImNrMzljOXZzcTAxazUzbHAxb3RqMzk3ZHoifQ.bk8S-keR8bJroxnGOhEXWA'
  }).addTo(myMap);
}

function drawRoutes(t){
  // var mapDiv = document.getElementById('mapcoll');
  // var toCheck = mapDiv.getAttribute('class');
  // if(!toCheck.includes('show')) {
  //   $(function() {
  //     $('#mapToggle').click();
  //   })
  // }
  clearMap();
  // var temptest= [
  //   {
  //     name:"pullman",
  //     airportID:"3944",
  //     latitude:"46.7439",
  //     longitude:"-117.11"
  //   },
  //   {
  //     name:"seatle",
  //     airportID:"3577",
  //     latitude:"47.449",
  //     longitude:"-122.309"
  //   },
  //   {
  //     name:"anchorage",
  //     airportID:"3774",
  //     latitude:"61.1744",
  //     longitude:"-149.996"
  //   }];
  var temptest = t["path"];
  var latLong = [];//[[46.7439,-117.11], [47.449,-122.309], [61.1744,-149.996]];
  temptest.forEach((line)=>{
    var marker = L.marker([line["latitude"], line["longitude"]]).addTo(myMap);
    var tt = [];
    tt.push(line["latitude"]);
    tt.push(line["longitude"]);
    marker.bindPopup("<b>" + line["name"] + "</b><br>airportID = " + line["airportID"]);
    //mapPopups.addLayer(marker);
    latLong.push(tt);
  });
  var polyline = L.polyline(latLong, {color: 'red'}).addTo(myMap);
  myMap.fitBounds(polyline.getBounds());
}
