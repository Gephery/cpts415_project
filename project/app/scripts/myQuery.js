function myQuery(mQuery){

    var mysql = require('mysql');

    var connection = mysql.createConnection({
        host: '98.146.204.39',
        port: '3306',
        user: 'readOnlyUser',
        password: 'cpts415-bigData',
        database: 'cs415db'
    });

    connection.connect(function(err){
        if(err){
            // console.log(err.code);
            // console.log(err.fatal);
        }
    });
    //CREATE TABLE routes ( airline VARCHAR(4), airlineID SMALLINT, srcAirport VARCHAR(5), srcAirportID SMALLINT, dstAirport VARCHAR(5), dstAirportID SMALLINT, codeshare CHAR(1), numStops TINYINT, equipment VARCHAR(30), routeID INT NOT NULL AUTO_INCREMENT, primary key(routeID), foreign key (srcAirportID) references airports(airportID), foreign key (dstAirportID) references airports(airportID), foreign key (airlineID) references airlines(airlineID) );

    //row =  
    connection.query(mQuery, function(err, rows, fields){
        if(err){
            // console.log("an error has** occurred with the query");
            // console.log(err);
            //return;
        }
        else {
            // console.log("query executed succesfully", rows);
            // console.log("fields", fields);
            // console.log(fields.length);
            var t = [];
            var q = [];
            for(var i = 0; i < fields.length; i++){
                t.push(fields[i].name);
            }
            // console.log("t", t);

            for(var i = 0; i < rows.length; i++){
                var r = [];
                for(var j = 0; j < t.length; j++){
                    r.push(rows[i][t[j]]);
                }
                q.push(r);
            }
            // console.log("q", q);
            updateTable(q, t);
        }
        $('#loading-modal').modal('hide');
        //return rows;
    });

    connection.end();
    //return row;
};//);