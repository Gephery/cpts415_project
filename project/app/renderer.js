var $ = require('jquery');
require('popper.js'); 
require('bootstrap');
require('bootstrap-select');
var L = require('leaflet');
const {ipcRenderer} = require('electron');
const mysql = require('mysql');
require('datatables.net-dt')();
const {updateTable, setTable} = require('./scripts/tableBuilder');

function tooltip() {
    $('[data-toggle="tooltip"]').tooltip();
}

function selectfix() {
    $('.sbn').on('change', (e) => { e.stopPropagation(); });
    $('.sb').on('change', (e) => {
        e = e.originalEvent;
        let text = e.target.selectedOptions[0].value;
        let g = text.split(' ');
        // Show only ones that are in the value field
        document.querySelectorAll('.sb').forEach((e) => {
            for (let i = 0; i < g.length; i++) {
                //console.log(`${e.id} vs ${g[i]}`);
                if (e.id == `sb-${g[i]}`) {
                    //console.log(`${e.id} show`);
                    $(e).collapse('show');
                    return;
                }
            }
            //console.log(`${e.id} hide`);
            $(e).collapse('hide');
        });
    });
    //$(location).attr('hash', e.target.selectedOptions[0].value);
    //$('select').on('change', (e) => { debugger;$(location).attr('hash', this.value); console.log(this.value); });
}

function _locationListBuild(er, res, fields) {
    let locListEl = '<option value="Santa">Spread</option>';
    res.forEach((e) => {
        locListEl += `<option value="${e["airportID"]}">${e["name"]} (${e["country"]}, ${e["city"]})</option>`;
    });
    $('#location-src').html(locListEl);
    $('#location-src').selectpicker('refresh');
    $('#location-dst').html(locListEl);
    $('#location-dst').selectpicker('refresh');

}

function locationListBuild() {
    var connection = mysql.createConnection({
        host: '98.146.204.39',
        port: '3306',
        user: 'readOnlyUser',
        password: 'cpts415-bigData',
        database: 'cs415db'
    });
    connection.query('SELECT airportID, name, city, country FROM airports WHERE 1=1', _locationListBuild);
}

ipcRenderer.on('asynchronous-reply', (event, arg) => {
    if (arg.isRoute) {
        //console.log('Route...');
        //console.log(arg);
        if(arg.found) {
            drawRoutes(arg);
            let fields = ['Route'];
            let airports = "Airports: ";
            let routes = "RouteIds: ";
            let i = 0;
            arg.path.forEach((q) =>{
                if (i == 0) 
                    airports += q.name;
                else
                    airports += ', ' + q.name;
                i++;
            });
            i = 0;
            arg.route.forEach((r)=>{
                if (i == 0)
                    routes += r.routeID;
                else
                    routes += ', ' + r.routeID;
                i++;
            });
            let s = [[airports], [routes]];
            updateTable(s , fields);
        }
    } else if (arg.isDebug) {
        //console.log(arg);
    } else if (arg.isSpread) {
        //console.log('Spread...');
        //console.log(arg);
        //let s = 'SELECT name, airportID, country, city FROM airports WHERE ';
        if (arg.found) {
            let fields = ['Airport ID', 'Name', 'Country', 'City'];
            let data = [];
            arg.path.forEach((d) => {
                data.push([d.airportID, d.name, d.country, d.city]);
            });
            updateTable(data, fields);
        }
    }
    if (!arg.found) {
        let fields = ["These Elves look familar."];
        let data = [["These aren't the Elves you're looking for."]];
        updateTable(data, fields);
    }
    $('#loading-modal').modal('hide');
})

$(window).on('load', () => {
    tooltip();
    selectfix();
    locationListBuild();
    setTable('#results-area');
    //updateTable([[1, 2],[3,'Test 4']], ['Item 1', 'Item 2']);
    // Test: seattle to NY, no limit on hops
    //$('#goButton').on('click', ()=> {console.log('Sending...');ipcRenderer.send('asynchronous-message', {isRoute: true, src: $('#location-src').val(), dst: $('#location-dst').val(), hops: $('#limit').val()});});
    //$('#goButton').on('click', ()=> {console.log('Sending...');ipcRenderer.send('asynchronous-message', {isSpread: true, src: $('#location-src').val(), hops: $('#limit').val()});});
    $('#results-area').DataTable();
});