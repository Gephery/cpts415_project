const {app, BrowserWindow, ipcMain} = require('electron');
const ejse = require('ejs-electron');
const path = require('path');
const url = require('url');
const mysql = require('mysql');
const {trip, spread, setup} = require('./scripts/findRoute.js');

const connection = mysql.createConnection({
    host: '98.146.204.39',
    port: '3306',
    user: 'readOnlyUser',
    password: 'cpts415-bigData',
    database: 'cs415db'
});
connection.connect((err) => {
    if (err) throw err('Oops: DB not doing good...');
    //console.log('DB connected');
});

setup();

ipcMain.on('asynchronous-message', (event, arg) => {
    if (arg.isRoute) {
        arg.event = event;
        trip(arg);
    } else if (arg.isSpread) {
        //console.log('isSpread');
        arg.event = event;
        spread(arg);
    }
});

// Dropdown notes
// | Type

let window;
function createWindow(){
    //creates window with the specified sizes. can add an icon here if we want
    window = new BrowserWindow({width:1040, height:700, webPreferences: {nodeIntegration: true, nodeIntegrationInWorker: true, sandbox: false}});

    ejse.data('test', 'Hi I am testing!');
    //ejse.data('results', [{type: "Name", value: "Goroka Airport"}, {type: "City", value: "Goroka"}]);

    //gets that shitty index.html loaded up
    window.loadURL(url.format({
        pathname: path.join(__dirname, 'index.ejs'),
        protocol: 'file:',
        slashes: true
    }));

    //dev tools. can remove before turning in.
    //window.webContents.openDevTools();

    //open this stuff
    window.on('closed', () => {
        window = null;
    });
}

//ready to go
app.on('ready', createWindow);

//close all this shit
app.on('window-all-closed', () => {
    if(process.platform !== 'darwin'){
        app.quit();
        connection.end((err) => {
            // connection is terminated 
            // Ensure all prev enqueued queries are still 
        });
    }
});
